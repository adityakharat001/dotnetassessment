﻿using MethodsDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodsDemo.Repository
{
    internal class UserRepository
    {
        internal void Login(User user)
        {
            if ((user.username == null || user.username == "") && (user.password == null || user.password == "")){
                Console.WriteLine("Fill your details!!");
            }
            else if (user.username == "admin" && user.password == "admin123")
            {
                Console.WriteLine("Your have successfully logged in!!");
            }
            else
            {
                Console.WriteLine("Enter valid credentials!!");
            }
        }
    }
}
