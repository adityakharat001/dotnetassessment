﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructDemoProject
{
    struct Course
    {
        public int courseId;
        public string courseName;

        public Course(int c_id, string c_name)
        {
            this.courseId = c_id;
            this.courseName = c_name;
        }
    }
}
