﻿using MVCWebApplication.Models;
using System.Security.Claims;

namespace MVCWebApplication.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        public bool AddUsers(User user);
        public bool DeleteUser(int id);
        public User UpdateUser(int id, User user);
        public User GetDetails(int id);
    }

}
