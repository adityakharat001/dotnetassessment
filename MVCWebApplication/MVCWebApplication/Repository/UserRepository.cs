﻿using MVCWebApplication.Models;

namespace MVCWebApplication.Repository
{
    public class UserRepository : IUserRepository
    {
        int index = 1;
        List<User> users;

        public UserRepository()
        {
            users = new List<User>();
        }

        public bool AddUsers(User user)
        {
            user.Id = index++;
            users.Add(user);
            return true;
        }

        public bool DeleteUser(int id)
        {
            User user = users.Find(u => u.Id == id);
            users.Remove(user);
            return true;
        }

        public User UpdateUser(int id, User updatedUser)
        {
            User user = users.Find(u => u.Id == id);
            user.Id = updatedUser.Id;
            user.Name = updatedUser.Name;
            user.Password = updatedUser.Password;
            user.City = updatedUser.City;
            return user;

        }

        public User GetDetails(int id)
        {
            User user = users.Find(u=>u.Id == id);
            return user;
        }

        public List<User> GetAllUsers()
        {
            return users;
        }
    }
}
