﻿using System.ComponentModel.DataAnnotations;

namespace MVCWebApplication.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string City { get; set; }

    }
}
