﻿using Microsoft.AspNetCore.Mvc;
using MVCWebApplication.Models;
using MVCWebApplication.Repository;

namespace MVCWebApplication.Controllers
{
    public class UserController : Controller
    {
        readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        //IUserRepository iuserRepository = (IUserRepository)new UserRepository();
        public ActionResult GetAllUsers()
        {
            List<User> users = _userRepository.GetAllUsers();
            return View(users);
        }

        public ActionResult Create()
        {
            return View();  
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            return View();  
        }
        [HttpPost]
        public ActionResult AddUser(User user)
        {
            _userRepository.AddUsers(user);
            return RedirectToAction("GetAllUsers");  
        }

        public ActionResult Delete(int id)
        {
            _userRepository.DeleteUser(id);
            return RedirectToAction("GetAllUsers");

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            User user = _userRepository.GetDetails(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult Edit(int id, User user)
        {
            _userRepository.UpdateUser(id, user);
            return RedirectToAction("GetAllUsers");
        }

        public ActionResult Details(int id)
        {
            User user = _userRepository.GetDetails(id);
            return View(user);
        }

        //User user = new User() { Id = 1, Name = "Adi", Password = "Adi123", City = "CBD" };
        //public IActionResult Index()
        //{
        //    return View(user);
        //}
        //public UserController(IUserRepository iuserRepository)
        //{
        //    this.iuserRepository = iuserRepository;
        //}


    }
}
