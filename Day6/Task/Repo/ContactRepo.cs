﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task.Exceptions;
using Task.Model;

namespace Task.Repo
{
    internal class ContactRepo
    {

        List<Contact> contacts;

        public ContactRepo()
        {
            contacts = new List<Contact>()
            {
                new Contact(){Name="Aditya", Address="CBD Belapur", City="Navi Mumbai", Phone="9876543210"},
                new Contact(){Name="Ajay", Address="CST", City="Navi Mumbai", Phone="9727427789"},
                new Contact(){Name="Madhu", Address="CST", City="Mumbai", Phone="9727427789"},
                new Contact(){Name="Rishi", Address="Dadar", City="Mumbai", Phone="9727427789"}
            };
        }

        public List<Contact> GetAllContacts()
        {
            return contacts;
        }
        public string AddContact(Contact contact)
        {
            var contactExists = GetContactByName(contact.Name);

            if(contactExists == null)
            {
                contacts.Add(contact);
                return $"Contact for {contact.Name} is added successfully.";

            }
            else
            {
                throw new ContactAlreadyExistsException($"Contact for {contact.Name} already exists.");
            }
            
        }

        public bool DeleteContact(string name)
        {
            var delContact = GetContactByName(name);
            return delContact != null ? contacts.Remove(delContact) : false;
            

        }

        public void UpdateContact(string name)
        {
            foreach(Contact item in contacts)
            {
                if (item.Name == name)
                {
                    Console.WriteLine("Enter new Address: ");
                    item.Address = Console.ReadLine();
                    Console.WriteLine("Enter new City: ");
                    item.City = Console.ReadLine();
                    Console.WriteLine("Enter new Phone number: ");
                    item.Phone = Console.ReadLine();
                    break;
                }
                
                
                
            }
                    Console.WriteLine("Contact does not exists.");
        }

        public List<Contact> GetContactByCity(string city)
        {
            List<Contact> CityContacts = new List<Contact>();
            CityContacts = contacts.Where(contact => contact.City == city).ToList();
            return CityContacts;
            
        }

        public Contact GetContactByName(string name)
        {
            var contact = contacts.Where(contact => contact.Name == name).FirstOrDefault();
            return contact;
            
        }


    }
}
