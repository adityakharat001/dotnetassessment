﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task.Exceptions
{
    internal class ContactAlreadyExistsException: Exception
    {
        public ContactAlreadyExistsException()
        {

        }
        public ContactAlreadyExistsException(string msg):base(msg)
        {

        }
    }
}
