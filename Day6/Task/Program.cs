﻿// See https://aka.ms/new-console-template for more information

using Task.Exceptions;
using Task.Model;
using Task.Repo;

ContactRepo contactRepo = new ContactRepo();
Contact contact = new Contact();
List<Contact> contacts = contactRepo.GetAllContacts();
foreach(Contact item in contacts)
{
    Console.WriteLine(item);
}

Console.WriteLine("---------------------------------------");
Console.WriteLine("ADD NEW CONTACT");
Console.WriteLine("Enter Name:");
contact.Name = Console.ReadLine();
Console.WriteLine("Enter Address:");
contact.Address = Console.ReadLine();
Console.WriteLine("Enter City:");
contact.City = Console.ReadLine();
Console.WriteLine("Enter Phone:");
contact.Phone =  Console.ReadLine();


try
{
    Console.WriteLine(contactRepo.AddContact(contact));
    foreach(Contact item in contacts)
    {
        Console.WriteLine(item);
    }
}
catch (ContactAlreadyExistsException caex)
{
    Console.WriteLine(caex.Message);
}

Console.WriteLine("---------------------------------------");
Console.WriteLine("ENTER NAME TO DELETE CONTACT");
var delContact = contactRepo.DeleteContact(Console.ReadLine());
if (!delContact)
{
    Console.WriteLine("Contact does not exist!");
}
foreach (Contact item in contacts)
{
    Console.WriteLine(item);
}

Console.WriteLine("---------------------------------------");
Console.WriteLine("ENTER NAME TO UPDATE CONTACT");
contactRepo.UpdateContact(Console.ReadLine());
foreach(Contact item in contacts)
{
    Console.WriteLine(item);
}
Console.WriteLine("---------------------------------------");
Console.WriteLine("FILTER CONTACT BY CITY");
Console.WriteLine("Enter city name to filter contacts:");
var citySort = contactRepo.GetContactByCity(Console.ReadLine());
foreach (Contact item in citySort)
{
    Console.WriteLine(item);
}
