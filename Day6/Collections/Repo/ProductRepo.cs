﻿using Collections.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections.Repo
{
    internal class ProductRepo
    {

        List<Product> products;
        public ProductRepo()
        {
        
            products = new List<Product>(){
                new Product(){Id=101,Name="Mobile",Price=22000},
                new Product(){Id=102,Name="Madhusudan",Price=20000}
            };

        }
        public List<Product> GetAllProducts()
        {
            return products;
        }

        public void AddProduct(Product product)
        {
            products.Add(product);
        }
    }
}
