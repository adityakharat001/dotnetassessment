﻿// See https://aka.ms/new-console-template for more information
using Exceptions;

MobileNumberValidation mobileNumberValidation = new MobileNumberValidation();
Console.WriteLine("Enter you mobile number: ");
try
{
    string mobNumber = Console.ReadLine();
    mobileNumberValidation.ValidateMobileNumber(mobNumber);
}
catch(InvalidMobileNumber ex)
{
    Console.WriteLine(ex.Message);
}
