﻿// See https://aka.ms/new-console-template for more information

using FileHandling.Model;
using FileHandling.Repo;
using FileHandling.Exception;

UserRepo userRepo = new UserRepo();
IUserRepo iuserRepo = (IUserRepo)userRepo;
User user = new User();

//bool isUserRegistered = iuserRepo.RegisterUser(new User() { Id = 1, Name = "Aditya", Age = 20 });

Console.WriteLine("-----------User Registration--------");
Console.WriteLine("Enter User Id: ");
user.Id = int.Parse(Console.ReadLine());

Console.WriteLine("Enter User Name: ");
user.Name = Console.ReadLine();

Console.WriteLine("Enter User Age: ");
user.Age = int.Parse(Console.ReadLine());

try
{

bool isUserRegistered = userRepo.RegisterUser(user);
    Console.WriteLine("Registration Successful");

}
catch(UserException ex)
{
    Console.WriteLine(ex.Message);
}


List<string> list = userRepo.ReadFromFile();
foreach (string item in list)
{
    Console.WriteLine(item);
}
