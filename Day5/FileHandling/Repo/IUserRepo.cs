﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repo
{
    internal interface IUserRepo
    {
        public bool RegisterUser(User user);
        public bool IsUserExist(string username);
    }
}
