﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHandling.Exception;

namespace FileHandling.Repo
{
    internal class UserRepo : IUserRepo, IFile
    {
        List<User> users;
        string filename = "userDatabase.txt";
        public UserRepo()
        {
        users = new List<User>();
        }
        public bool IsUserExist(string username)
        {
            bool isUserRegistered = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowLine;
                while((rowLine = sr.ReadLine()) != null)
                {
                    string[] rowValues = rowLine.Split(",");

                    foreach(var individualVal in rowValues)
                    {
                        if (rowValues[1] == username)
                        {
                            isUserRegistered = true;
                        }
                    }
                    
                    
                }
            }
            return isUserRegistered;
        }

        public List<string> ReadFromFile()
        {
            List<string> userList;
            using (StreamReader sr = new StreamReader(filename))
            {
                userList = new List<string>();
                while(sr.Peek() >= 0)
                {
                    userList.Add(sr.ReadLine());
                }
                return userList;
            }
        }

        public bool RegisterUser(User user)
        {

            if (!IsUserExist(user.Name))
            {
                WriteToFile(user, filename);
                users.Add(user);
                return true;
            }
            else
            {
                throw new UserException();
                return false;   
            }
           

        }

        public void WriteToFile(User user, string filename)
        {
            using (StreamWriter sw = new StreamWriter(filename, true))
            {
                sw.Write($"{user}");
            }
        }
    }
}
