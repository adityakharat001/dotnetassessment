﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repo
{
    internal interface IFile
    {
        public void WriteToFile(User user,string path);
        public List<string> ReadFromFile();
    }
}
