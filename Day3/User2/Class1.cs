﻿using AccessModifiers;

namespace User2
{
    public class User2:User
    {
        public void Display()
        {
            Console.WriteLine(this.name);
            Console.WriteLine(this.password);
        }
    }
}