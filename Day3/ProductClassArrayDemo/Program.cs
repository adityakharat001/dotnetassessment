﻿// See https://aka.ms/new-console-template for more information
using ProductClassArrayDemo.Model;
using ProductClassArrayDemo.Repo;

ProductRepo productRepo = new ProductRepo();
Product[] allProducts = productRepo.GetAllProducts();

Console.WriteLine("***************************************************");

Console.WriteLine("All Products: ");
foreach (Product item in allProducts)
{
    Console.WriteLine(item);
}

Console.WriteLine("***************************************************");

Console.WriteLine("Products with 'Mobile' Category: ");
Product[] mobileCat = productRepo.MobileCategory();
foreach(Product item in mobileCat)
{
    Console.WriteLine(item);
}

Console.WriteLine("***************************************************");

Console.WriteLine("Updated Values table: ");
Console.WriteLine("Enter Name of product to be updated: ");
string productName = Console.ReadLine();
Console.WriteLine("Enter new price of product to be updated: ");
int updatedPrice = int.Parse(Console.ReadLine());

Product[] updatedProducts = productRepo.UpdateElement(productName , updatedPrice);
foreach (Product item in updatedProducts)
{
    Console.WriteLine(item);
}

Console.WriteLine("***************************************************");

Console.WriteLine("After Deleting Values table: ");
productName = Console.ReadLine();
Product[] deletedProducts = productRepo.DeleteElement(productName);
foreach (Product item in deletedProducts)
{
    Console.WriteLine(item);
}
