﻿using ProductClassArrayDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductClassArrayDemo.Repo
{
    internal class ProductRepo
    {
        Product[] products;

        public ProductRepo()
        {
            products = new Product[]
            {
                new Product("Samsung", "Mobile", 20000, 4.2f),
                new Product("Oppo", "Mobile", 18000, 4.2f),
                new Product("Dell", "Laptop", 50000, 4.1f),
                new Product("Samsung", "TV", 40000, 4.3f)

            };
        }
        public Product[] GetAllProducts() {
            return products;
        }
        
        public Product[] MobileCategory()
        {

            return Array.FindAll(products, item => item.Category == "Mobile");
        }
        public Product[] UpdateElement(string name, int price)
        {
            foreach(Product item in products)
            {
                if(item.Name == name)
                {
                    item.Price = price;
                    
                }
               
            }
            return products;
            
        }
        public Product[] DeleteElement(string name)
        {
            return Array.FindAll(products, item => item.Name != name);
        }
    }
}
