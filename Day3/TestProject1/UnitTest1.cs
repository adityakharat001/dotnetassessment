namespace TestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var tempFaren = 91.22;
            var expected = 32.9;

            var tempCel = FarenToCel.FarenhietToCelcius(tempFaren);
            Assert.Equal(expected, tempCel);
        }
    }
}