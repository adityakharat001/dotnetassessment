namespace TestProject2
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var tempFaren = 91.22;
            var expected = 32.9;

            var tempCel = FarenToCel.FarenhietToCelcius(tempFaren);
            Assert.AreEqual(expected, tempCel);
        }
    }
}