﻿using Microsoft.AspNetCore.Mvc;
using MVCWebAppWithDB.Models;
using MVCWebAppWithDB.Repository;
using MVCWebAppWithDB.Services;

namespace MVCWebAppWithDB.Controllers
{
    public class UserController : Controller
    {
        readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult GetAllUsers()
        {
            List<User> users = _userService.GetAllUsers();
            return View(users);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(User user)
        {
            _userService.AddUsers(user);
            return RedirectToAction("GetAllUsers");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            User user = _userService.GetUsersById(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult Edit(int id, User user)
        {
            _userService.UpdateDetails(id, user);
            return RedirectToAction("GetAllUsers");
        }

        public ActionResult Delete(User user)
        {
            User userNew = _userService.GetUsersById(user.Id);
            _userService.DeleteDetails(userNew);
            return RedirectToAction("GetAllUsers");
        }

        public ActionResult Details(int id)
        {
            User user = _userService.GetDetails(id);
            return View(user);
        }

        public IActionResult Index()
        {
            return View();
        }


    }
}
