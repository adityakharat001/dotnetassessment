﻿using System.ComponentModel.DataAnnotations;

namespace MVCWebAppWithDB.Models
{
    public class User
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string City { get; set; }

        public bool IsDeleted { get; set; }  
    }
}
