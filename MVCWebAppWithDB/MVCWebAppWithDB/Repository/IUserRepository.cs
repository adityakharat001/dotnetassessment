﻿using MVCWebAppWithDB.Models;

namespace MVCWebAppWithDB.Repository
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        bool AddUsers(User user);
        bool UpdatedDetails(User user);
        User GetUsersById(int id);
        bool DeleteDetails(User user);
        User GetDetails(int id);
    }
}
