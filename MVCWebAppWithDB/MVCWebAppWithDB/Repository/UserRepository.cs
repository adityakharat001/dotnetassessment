﻿using Microsoft.EntityFrameworkCore;
using MVCWebAppWithDB.Context;
using MVCWebAppWithDB.Models;

namespace MVCWebAppWithDB.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly UserDbContext _userDbContext;
        public UserRepository(UserDbContext userDbContext)
        {
            _userDbContext = userDbContext;
        }

        public List<User> GetAllUsers()
        {
            return _userDbContext.users.Where(x=>x.IsDeleted!=true).ToList();
        }

        public bool AddUsers(User user)
        {
            _userDbContext.users.Add(user);
            return _userDbContext.SaveChanges() == 1 ? true : false;
        }

        public bool UpdatedDetails(User user)
        {
            _userDbContext.Entry<User>(user).State = EntityState.Modified;
            _userDbContext.SaveChanges();
            return true;

        }



        public User GetUsersById(int id)
        {
            User user = _userDbContext.users.Where(u => u.Id == id).FirstOrDefault();
            return user;
        }

        public bool DeleteDetails(int id)
        {
            user.IsDeleted = true;
            _userDbContext.Entry<User>(user).State = EntityState.Modified;
            _userDbContext.SaveChanges();
            return true;
        }

        public User GetDetails(int id)
        {
            User user = _userDbContext.users.Where(u => u.Id == id).FirstOrDefault();
            return user;
        }
    }
}
