﻿using MVCWebAppWithDB.Models;
using MVCWebAppWithDB.Repository;

namespace MVCWebAppWithDB.Services
{
    public class UserService: IUserService
    {
        readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        public List<User> GetAllUsers()
        {
            List<User> users = _userRepository.GetAllUsers();
            return users;
        }
        public bool AddUsers(User user)
        {
            _userRepository.AddUsers(user);
            return true;
        }

        public bool UpdateDetails(int id, User user)
        {
            _userRepository.UpdatedDetails(user);
            return true;
        }

        public User GetUsersById(int id)
        {
            User user =  _userRepository.GetUsersById(id);
            return user;
        }

        public bool DeleteDetails(User user)
        {
            _userRepository.DeleteDetails(user);
            return true;
        }

        public User GetDetails(int id)
        {
           User user =  _userRepository.GetDetails(id);
            return user;
        }

        
    }
}
