﻿using MVCWebAppWithDB.Models;

namespace MVCWebAppWithDB.Services
{
    public interface IUserService
    {
        List<User> GetAllUsers();
        bool AddUsers(User user);
        bool UpdateDetails(int id, User user);
        User GetUsersById(int id);
        bool DeleteDetails(User user);
        User GetDetails(int id);
    }
}
