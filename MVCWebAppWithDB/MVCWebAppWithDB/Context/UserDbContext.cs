﻿using Microsoft.EntityFrameworkCore;
using MVCWebAppWithDB.Models;

namespace MVCWebAppWithDB.Context
{
    public class UserDbContext:DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> Context): base(Context)
        {

        }

        public DbSet<User> users { get; set; }
    }
}
