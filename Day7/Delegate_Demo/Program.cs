﻿// See https://aka.ms/new-console-template for more information
namespace Delegate_Demo
{
    class Delegate
    {
        public static void Main()
        {
            Console.WriteLine("Delegates");
            Console.WriteLine("Enter Num1:");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter Num2:");
            int b = int.Parse(Console.ReadLine());

            Action<int, int> ds = (int num1, int num2) =>
            {
                Console.WriteLine(num1 * num2);
            };
            ds(a, b);

            Func<int, int, int> ds1 = (a, b) => a * b;
            int product = ds1(a, b);
            Console.WriteLine(product);
            string text = "Hello world";
            Predicate<string> sc = (string text) =>
        {
            Console.WriteLine("Enter string to search: ");
            string cText = Console.ReadLine();
            if (text.Contains(cText))
            {
                return true;
            }
            else
            {
                return false;
            }
        };
            Console.WriteLine(sc(text));

        }

        //public static int Multiply(int num1, int num2)
        //{
        //    return num1 * num2;

        //}

        //public static void Multiply2(int num1, int num2)
        //{
        //    Console.WriteLine(num1 * num2);
        //}
        //public static bool StrContains(string text)
        //{
        //    Console.WriteLine("Enter string to search: ");
        //    string cText = Console.ReadLine();
        //    if (text.Contains(cText))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
    }
}