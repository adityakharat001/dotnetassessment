﻿// See https://aka.ms/new-console-template for more information
using JoinDemo.Model;

List<Product> products;
List <Stock> stocks = new List<Stock>();

products = new List<Product>()
{
    new Product{ Id=1,Name="Mobile", Price=15000},
    new Product{ Id=2,Name="TV", Price=40000},
    new Product{ Id=3,Name="Laptop", Price=55000},
    new Product{ Id=4,Name="Keyboard", Price=1000}

};

stocks = new List<Stock>()
{
    new Stock{Id=1, Quantity=22},
    new Stock{Id=2, Quantity=7},
    new Stock{Id=3, Quantity=10},
    new Stock{Id=4, Quantity=14}
};

var joinResult = from stock in stocks
                 join product in products
                 on stock.Id equals product.Id
                 select new
                 {
                     ProductName = product.Name,
                     Quantity = stock.Quantity,
                     TotalPrice = product.Price * stock.Quantity
                 };


foreach (var item in joinResult)
{
    Console.WriteLine(item);
}