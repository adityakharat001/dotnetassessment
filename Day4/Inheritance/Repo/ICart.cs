﻿using Inheritance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance.Repo
{
    internal interface ICart
    {
        string AddToCart(Product product);
        List<Product> RemoveFromCart(string name);  
    }
}
