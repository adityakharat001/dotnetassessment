﻿using Inheritance.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance.Repo
{
    internal class ProductRepo : IOrder, ICart
    {
        List<Product> products;

        public ProductRepo()
        {
            products = new List<Product>();
        }
        public string AddToCart(Product product)
        {
            products.Add(product);
            return $"Name: {product.Name}\t Price: {product.Price}";
        }

        public string BookOrder()
        {
            return "Your order is booked";
        }

        //public string CancelOrder()
        //{
        //    return "Your order is cancelled";

        //}

        public List<Product> RemoveFromCart(string name)
        {
            var removeProduct = products.Find(item => item.Name == name);
            products.Remove(removeProduct);
            return products;
        }
        public void DisplayProduct()
        {
            foreach (Product item in products)
            {
                Console.WriteLine($"Name:{item.Name}\t Price: {item.Price}");
            }
        }


    }
}
