﻿// See https://aka.ms/new-console-template for more information

using Inheritance.Model;
using Inheritance.Repo;

ProductRepo productRepo = new ProductRepo();
Product pr1 = new Product("Keyboard", 900);
productRepo.AddToCart(pr1);
Product pr2 = new Product("Monitor", 6000);
productRepo.AddToCart(pr2);
Product pr3 = new Product("Mouse", 700);
productRepo.AddToCart(pr3);
Product pr4 = new Product("Laptop", 60000);
productRepo.AddToCart(pr4);
Product pr5 = new Product("Desktop", 90000);
productRepo.AddToCart(pr5);
Console.WriteLine("------------------------------------------------------------");
Console.WriteLine("Products: ");
productRepo.DisplayProduct();
Console.WriteLine("------------------------------------------------------------");
Console.WriteLine("Add Product and Price: ");
string prName = Console.ReadLine();
double price = double.Parse(Console.ReadLine());
Product productVal = new Product(prName, price);
productRepo.AddToCart(productVal);
productRepo.DisplayProduct();
Console.WriteLine("------------------------------------------------------------");
Console.WriteLine("Enter Product name to remove from cart: ");
prName = Console.ReadLine();
productRepo.RemoveFromCart(prName);
productRepo.DisplayProduct();
Console.WriteLine("------------------------------------------------------------");
Console.WriteLine(productRepo.BookOrder());
