﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExpression
{
    internal class PasswordVal
    {

        public bool validatePass(string pass)
        {
            string pattern = @"^[A-Za-z0-9!@#%&]{8,}$";
            if (pass != null)
            {
                return Regex.IsMatch(pass, pattern);
            }
            else return false;
        }
    }
}
