﻿// See https://aka.ms/new-console-template for more information
using System.Data;
using System.Data.SqlClient;

Connection();

static void Connection()
{
    string cs = "Data source = DESKTOP-IJKQSF0 ; Database = StudentDb; integrated security=true";
    //SqlConnection con = new SqlConnection(cs);
    //con.Open();
    //if(con.State == ConnectionState.Open)
    //{
    //    Console.WriteLine("Connection Established Successfully");
    //}
    //else
    //{
    //    Console.WriteLine("Connection Failed");
    //}

    try
    {
        using (SqlConnection con = new SqlConnection(cs))
        {
            string query = "select * from StudentTbl";
            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"ID: {reader["Student_ID"]}\t StudentName: {reader["Student_Name"]}\t City: {reader["City"]}");
            }
            if (con.State == ConnectionState.Open)
            {
                Console.WriteLine("Connection Established Successfully");
            }
        }
    }catch(SqlException ex)
    {
        Console.WriteLine(ex.Message);
    }
}