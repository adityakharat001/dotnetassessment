﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskWithDataAdapter.Model
{
    internal class Student
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\t Name: {Name}\t City: {City}";
        }

    }
}
