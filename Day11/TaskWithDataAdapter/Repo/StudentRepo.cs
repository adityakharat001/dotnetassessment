﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskWithDataAdapter.Model;

namespace TaskWithDataAdapter.Repo
{
    internal class StudentRepo
    {
        SqlConnection conn = null;
        SqlCommand cmd = null;

        public StudentRepo()
        {
            string cs = "Data source = DESKTOP-IJKQSF0 ; Database = StudDb; integrated security=true";
            conn = new SqlConnection(cs);

        }

        public List<Student> GetAllStudentsByDS()
        {
            List<Student> students = new List<Student>();
            string query = "select * from StudentTbl";
            cmd = new SqlCommand(query, conn);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(query, conn);
            DataSet ds = new DataSet();

            conn.Open();
            dataAdapter.Fill(ds,"student_table");
            foreach(DataRow row in ds.Tables["student_table"].Rows)
            {
                Console.WriteLine($"Id: {row["StudentId"]}\t Name: {row["StudentName"]}\t City: {row["City"]}");
            }
            
            conn.Close();
            return students;
        }

        public List<Student> GetAllStudentsByDT()
        {
            List<Student> students = new List<Student>();
            string query = "select * from StudentTbl";
            cmd = new SqlCommand(query, conn);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(query, conn);
            DataTable dt = new DataTable();

            conn.Open();
            dataAdapter.Fill(dt);
            foreach (DataRow row in dt.Rows)
            {
                Console.WriteLine($"Id: {row["StudentId"]}\t Name: {row["StudentName"]}\t City: {row["City"]}");
            }

            conn.Close();
            return students;
        }

    }
}
