﻿// See https://aka.ms/new-console-template for more information
using TaskWithDataAdapter.Model;
using TaskWithDataAdapter.Repo;

StudentRepo studentRepo = new StudentRepo();
Console.WriteLine("----------Using DataSet and Data Adapter------------");
studentRepo.GetAllStudentsByDS();
Console.WriteLine("----------Using DataTable and Data Adapter------------");
studentRepo.GetAllStudentsByDT();

Console.WriteLine("Enter Student Id: ");
