﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task.Model;

namespace Task.Repo
{
    internal class StudentRepo
    {
        SqlConnection conn = null;
        SqlCommand cmd = null;

        public StudentRepo()
        {
            string cs = "Data source = DESKTOP-IJKQSF0 ; Database = StudDb; integrated security=true";
             conn = new SqlConnection(cs);

        }

        public List<Student> GetAllStudents()
        {
            List<Student> students = new List<Student>();   
            string query = "select * from StudentTbl";
            cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader= cmd.ExecuteReader();
            while (reader.Read())
            {
                Student student = new Student();
                student.Id = (int)reader["StudentId"];
                student.Name = (string)reader["StudentName"];
                student.City = (string)reader["City"];
                students.Add(student);
            }
            conn.Close();
            return students;
        }

        public void AddStudents(Student student)
        {
            string query = $"Insert into StudentTbl values(@id,@name,@city)";
            cmd = new SqlCommand(query, conn);
            int StudentId = student.Id;
            string StudentName = student.Name;
            string City = student.City;
            cmd.Parameters.AddWithValue("@id", StudentId);
            cmd.Parameters.AddWithValue("@name", StudentName);
            cmd.Parameters.AddWithValue("@city", City);
            conn.Open();
            int insertResult = cmd.ExecuteNonQuery();
            if (insertResult > 0)
            {
                Console.WriteLine("Student added successfully");
            }
            else
            {
                Console.WriteLine("Student data not added");
            }
            conn.Close();
        }

        public void UpdateStudentData(Student student)
        {
            string query = "Update StudentTbl set StudentName = @name,City = @city where StudentId = @id";
            cmd = new SqlCommand(query, conn);
            int StudentId = student.Id;
            string StudentName = student.Name;
            string City = student.City;
            cmd.Parameters.AddWithValue("@id", StudentId);
            cmd.Parameters.AddWithValue("@name", StudentName);
            cmd.Parameters.AddWithValue("@city", City); conn.Open();
            int updatedResult = cmd.ExecuteNonQuery();
            if (updatedResult > 0)
            {
                Console.WriteLine("Student data updated successfully");
            }
            else
            {
                Console.WriteLine("Student data not updated");
            }
            conn.Close();
        }

        public void DeleteStudentData(Student student)
        {
            string query = "Delete from StudentTbl where StudentId = @id";
            cmd = new SqlCommand(query, conn);
            int StudentId = student.Id;
            cmd.Parameters.AddWithValue("@id", StudentId);
            conn.Open();
            int deletedResult = cmd.ExecuteNonQuery();
            if (deletedResult > 0)
            {
                Console.WriteLine("Student data deleted successfully");
            }
            else
            {
                Console.WriteLine("Student data not deleted");
            }
            conn.Close();
        }

        public void GetStudentsById(int id)
        {
            string query = "select StudentTbl.StudentId, StudentTbl.StudentName,StudentTbl.Dept_Id,DeptTbl.DeptName from StudentTbl join DeptTbl on StudentTbl.Dept_Id = DeptTbl.DeptId where StudentTbl.StudentId = @id";
            cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@Id", id);
            conn.Open();
            SqlDataReader reader= cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine($"StudentId: {reader["StudentId"]}\t StudentName: {reader["StudentName"]}\t DeptId: {reader["Dept_Id"]}\t DeptName: {reader["DeptName"]}");
            }
            conn.Close();
        }



    }
}
