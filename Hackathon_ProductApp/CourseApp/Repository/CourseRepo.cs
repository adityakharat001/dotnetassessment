﻿using CourseApp.Model;
using System.Data.SqlClient;
using System.Text;

namespace CourseApp.Repository
{
    internal class CourseRepo
    {
        List<Course> courseInCart;
        SqlConnection conn = null;
        SqlCommand cmd = null;
        public CourseRepo()
        {
            courseInCart = new List<Course>();
            string cs = "Data source = DESKTOP-IJKQSF0 ; Database = ProductDb; integrated security=true";
            conn = new SqlConnection(cs);

        }

        //List<Course> courses = new List<Course>()
        //{
        //    new Course(){Coursename = "The Web Development Bootcamp 2022",Author="Colt Steele", Category="Web Development", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "The Complete 2022 Web Development Bootcamp",Author="Angela Yu", Category="Web Development", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "Angular-The Complete Guide 2022",Author="Max Schwaz", Category="Web Development", Price=2500,Rating=4.5f},
        //    new Course(){Coursename = "Investing In Stocks Complete Guide",Author="Steeve Ballinger", Category="Investing and Trading", Price=2000,Rating=4.6f},
        //    new Course(){Coursename = "Stocks Trading For Beginners",Author="Indian Insight", Category="Investing and Trading", Price=3000,Rating=4.5f},
        //    new Course(){Coursename = "Technical Analysis Mastery Stocks Trading",Author="Haris Hayat", Category="Investing and Trading", Price=3400,Rating=4.7f},
        //    new Course(){Coursename = "The 2022 Blender Primer",Author="Seba Milano", Category="3D and Animation", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "Ultimate Blender, 3D Redering And Animations",Author="Alex Cordebard", Category="3D and Animation", Price=3200,Rating=4.6f},
        //    new Course(){Coursename = "Ultimate Fitness Home Workout 2022",Author="Andrew Snagger", Category="Fitness", Price=2000,Rating=4.7f},
        //    new Course(){Coursename = "Meditation",Author="Dr. menon", Category="Fitness", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "Yoga, A Peaceful Therapy",Author="stanley", Category="Fitness", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "Learn Piano 2022",Author="Liew Paun", Category="Musical Instruments", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "Learn Guitar 2022",Author="James Hugh", Category="c", Price=3000,Rating=4.7f},
        //    new Course(){Coursename = "Learn Ukelele",Author="Andrew Watson", Category="Musical Instruments", Price=3000,Rating=4.7f},
        //};
        public void DisplayMenu()
        {
            if (courseInCart.Count == 0)
            {
                Console.WriteLine("1. Course Categories");
                Console.WriteLine("2. View Cart");
                Console.WriteLine("0. Exit");



            }
            else
            {
                Console.WriteLine("1. Course Categories");
                Console.WriteLine("2. View Cart");
                Console.WriteLine("3. Generate Bill");
                Console.WriteLine("0. Exit");
            }

        }

        public void Menu1()
        {
            var menuChoice = int.Parse(Console.ReadLine());
            if (menuChoice == 1)
            {
                DisplayCategories();
                Console.WriteLine("Enter your choice: ");
                int categoryNum = int.Parse(Console.ReadLine());
               
                switch (categoryNum)
                {
                    case 1:

                        List<Course> selcCourse = GetCourseByCategory("Web Development").ToList();
                        SelectedCourse(selcCourse);
                        break;

                    case 2:
                        selcCourse = GetCourseByCategory("Investing and Trading").ToList();
                        SelectedCourse(selcCourse);
                        break;

                      
                    case 3:
                        selcCourse = GetCourseByCategory("3D and Animation").ToList();
                        SelectedCourse(selcCourse);
                        break;
                        
                    case 4:
                        selcCourse = GetCourseByCategory("Fitness").ToList();
                        SelectedCourse(selcCourse);
                        break;
                        
                    case 5:
                        selcCourse = GetCourseByCategory("Musical Instruments").ToList();
                        SelectedCourse(selcCourse);
                        break;
                        
                    default:
                        Console.WriteLine("Enter Valid Input");
                        break;
                }

            }
            else if (menuChoice == 2)
            {
                var cart = CartContent();
                if (cart != null)
                {
                    Console.WriteLine("----------------CART----------------");
                    foreach (var item in cart)
                    {
                        if (item != null)
                        {
                            Console.WriteLine(item);

                        }

                    }
                }
                else
                {
                    Console.WriteLine("Your cart is empty");


                }

            }
            else if (menuChoice == 3)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("$");
                sb.Append("_");
                sb.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                Console.WriteLine(sb);
                int total = 0;
                foreach (Course item in courseInCart)
                {
                    total += item.Price;
                }
                double totalGST_Price = total + (total * 0.8);
                Console.WriteLine($"Total price of items in cart:: Rs.{total}");
                Console.WriteLine($"Total price of items in cart with GST :: Rs.{totalGST_Price}");
            }
            else if (menuChoice == 0)
            {
                Environment.Exit(0);
            }
        }

        

        public void DisplayCategories()
        {
            Console.WriteLine("----------------------------------------");
            Console.WriteLine("1. Web Development");
            Console.WriteLine("2. Investing and Trading");
            Console.WriteLine("3. 3D and Animation");
            Console.WriteLine("4. Fitness");
            Console.WriteLine("5. Musical Instruments");
            Console.WriteLine("------------------------------------------------");




        }

        public List<Course> CartContent()
        {
            Console.WriteLine("--------------------");
            return courseInCart;


        }


        public List<Course> GetCourseByCategory(string courseCat)
        {
            Course course = new Course();
            courseCat = course.Category;
            List<Course> courses = new List<Course>();
            string query = "select CourseName, Author, Price, Rating from ProductTbl where Category = @courseCategory";
            cmd = new SqlCommand(query, conn);
            string Category = course.Category;
            cmd.Parameters.AddWithValue("@courseCategory", courseCat);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                course.Coursename = (string)reader["CourseName"];
                course.Author = (string)reader["Author"];
                course.Category = (string)reader["Category"];
                course.Price = (int)reader["Price"];
                course.Rating = (double)reader["Rating"];
                courses.Add(course);
            }
            conn.Close();
            return courses;


            
        }

        public void SelectedCourse(List<Course> selcCourse)
        {
            int c_index = 1;
            foreach (var course in selcCourse)
            {
                Console.WriteLine($"{c_index}: {course.Coursename}");
                Console.WriteLine("------------------------------------------");
                c_index++;
            }
            Console.WriteLine("Select a course:");
            int selcCourseNum = int.Parse(Console.ReadLine());
            var selectedItem = selcCourse[selcCourseNum - 1];
            Console.WriteLine(selectedItem);

            CourseOptions(selectedItem);
            Console.WriteLine("---------------------------------------------------");


        }

        //public void AddStudents(Student student)
        //{
        //    string query = $"Insert into StudentTbl values(@id,@name,@city)";
        //    cmd = new SqlCommand(query, conn);
        //    int StudentId = student.Id;
        //    string StudentName = student.Name;
        //    string City = student.City;
        //    cmd.Parameters.AddWithValue("@id", StudentId);
        //    cmd.Parameters.AddWithValue("@name", StudentName);
        //    cmd.Parameters.AddWithValue("@city", City);
        //    conn.Open();
        //    int insertResult = cmd.ExecuteNonQuery();
        //    if (insertResult > 0)
        //    {
        //        Console.WriteLine("Student added successfully");
        //    }
        //    else
        //    {
        //        Console.WriteLine("Student data not added");
        //    }
        //    conn.Close();
        //}

        public void CourseOptions(Course selcItem)
        {
            if (courseInCart.Count == 0)
            {

                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("1. Add Course");
                Console.WriteLine("2. Buy Course");

                Console.WriteLine("Enter a choice: ");
                var buyCartOption = int.Parse(Console.ReadLine());
                if (buyCartOption == 1)
                {
                    //foreach (Course item in courses)
                    //{
                    //    if (item.Coursename == selcItem.Coursename && selcItem.Coursename != null)
                    //    {
                    //        Console.WriteLine($"{selcItem.Coursename} added to cart successfully");
                    //        Console.WriteLine("Coursename\t Author\t Rating\t  Category\t Price");
                    //        courseInCart.Add(item);
                    //    }
                    //}

                }
                else if (buyCartOption == 2)
                {

                    courseInCart = courseInCart.Where(c => c.Coursename == selcItem.Coursename).ToList();
                    Console.WriteLine($"The price for course ::{selcItem.Coursename}:: is Rs.{selcItem.Price}");
                    var gst_Price = selcItem.Price + (selcItem.Price * 0.18);
                    Console.WriteLine($"Order price with GST is Rs.{gst_Price}");
                }
                foreach (Course item in courseInCart)
                {

                    Console.WriteLine(item);
                };

            }
            else
            {

                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("1. Add Course");
                Console.WriteLine("2. Buy Course");
                Console.WriteLine("3. Remove Course");


                Console.WriteLine("Enter a choice: ");
                var buyCartOption = int.Parse(Console.ReadLine());
                if (buyCartOption == 1)
                {
                    //foreach (Course item in courses)
                    //{
                    //    if (item.Coursename == selcItem.Coursename && selcItem.Coursename != null)
                    //    {
                    //        Console.WriteLine($"{selcItem.Coursename} added to cart successfully");
                    //        Console.WriteLine("Coursename\t Author\t Rating\t  Category\t Price");
                    //        courseInCart.Add(item);
                    //    }
                    //}

                }
                else if (buyCartOption == 2)
                {

                    courseInCart = courseInCart.Where(c => c.Coursename == selcItem.Coursename).ToList();
                    Console.WriteLine($"The price for course ::{selcItem.Coursename}:: is Rs.{selcItem.Price}");
                    var gst_Price = selcItem.Price + (selcItem.Price * 0.18);
                    Console.WriteLine($"Order price with GST is Rs.{gst_Price}");
                }
                else if (buyCartOption == 3)
                {
                    foreach (Course item in courseInCart)
                    {
                        if (item.Coursename == selcItem.Coursename)
                        {
                            courseInCart.Remove(item);
                            Console.WriteLine($"{selcItem.Coursename} removed to cart successfully");
                        }

                    }
                    Console.WriteLine($"{selcItem.Coursename} does not exist in cart");

                }

                foreach (Course item in courseInCart)
                {
                    Console.WriteLine(item);
                }

            }



        }

       
    }
}