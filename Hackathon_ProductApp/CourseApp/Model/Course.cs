﻿namespace CourseApp.Model
{
    internal class Course
    {
        public string Coursename { get; set; }
        public int Price { get; set; }
        public string Author { get; set; }
        public double Rating { get; set; }
        public string Category { get; set; }

        public override string ToString()
        {

            return $"{Coursename}\t {Author}\t {Rating}\t {Category}\t Rs.{Price}";
        }
    }
}
