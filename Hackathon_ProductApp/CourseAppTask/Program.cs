﻿// See https://aka.ms/new-console-template for more information


using CourseAppTask.Model;
using CourseAppTask.Repository;
using System.Text;

Console.WriteLine("-------------------COURSE APP-------------------");
Console.WriteLine("Enter your username: ");
string userName = Console.ReadLine();
Console.WriteLine($"Hello, {userName}!");

CourseRepo courseRepo = new CourseRepo();
Course course = new Course();


while (true)
{

    Console.WriteLine("----------------------Menu----------------------");
    courseRepo.DisplayMenu();
    Console.WriteLine("Enter your choice: ");
    int menuChoice = int.Parse(Console.ReadLine());


    if (menuChoice == 1)
    {
        courseRepo.DisplayCategories();
        Console.WriteLine("Enter your choice: ");
        int categoryNum = int.Parse(Console.ReadLine());

        switch (categoryNum)
        {
            case 1:

                List<Course> selcCourse = courseRepo.GetCourseByCategory("Web Development").ToList();
                courseRepo.SelectedCourse(selcCourse);
                break;

            case 2:
                selcCourse = courseRepo.GetCourseByCategory("Investing and Trading").ToList();
                courseRepo.SelectedCourse(selcCourse);
                break;


            case 3:
                selcCourse = courseRepo.GetCourseByCategory("3D and Animation").ToList();
                courseRepo.SelectedCourse(selcCourse);
                break;

            case 4:
                selcCourse = courseRepo.GetCourseByCategory("Fitness").ToList();
                courseRepo.SelectedCourse(selcCourse);
                break;

            case 5:
                selcCourse = courseRepo.GetCourseByCategory("Musical Instruments").ToList();
                courseRepo.SelectedCourse(selcCourse);
                break;

            default:
                Console.WriteLine("Enter Valid Input");
                break;
        }

    }
    else if (menuChoice == 2)
    {
        var cart = courseRepo.CartContent();
        if (cart != null)
        {
            Console.WriteLine("----------------CART----------------");
            Console.WriteLine($"Coursename\t Author\t Rating\t Category\t Price");
            Console.WriteLine("------------------------------------");
            foreach (var item in cart)
            {
                Console.WriteLine(item);
            }
        }
        else
        {
            Console.WriteLine("Your cart is empty");
        }

    }
    else if (menuChoice == 3)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("$");
        sb.Append("_");
        sb.Append(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
        Console.WriteLine(sb);
        Console.WriteLine("----------------CART----------------");
        Console.WriteLine($"Coursename\t Author\t Rating\t Category\t Price");
        Console.WriteLine("------------------------------------");
        var cart = courseRepo.CartContent();
        foreach (var item in cart)
        {
            Console.WriteLine(item);
        }
        Console.WriteLine("------------------------------------");
        courseRepo.GenerateBill();
    }
    else if (menuChoice == 0)
    {
        Environment.Exit(0);
    }

}



