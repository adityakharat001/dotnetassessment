﻿using CourseAppTask.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseApp.Repository
{
    internal interface ICourseInterface
    {
        public List<Course> CourseOptions(Course selcItem);
    }
}
