﻿using CourseAppTask.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAppTask.Repository
{
    internal class CourseRepo
    {
        List<Course> cartList = new List<Course>();
         
        SqlConnection conn = null;
        SqlCommand cmd = null;
        public CourseRepo()
        {
            string cs = "Data source = DESKTOP-IJKQSF0 ; Database = ProductDb; integrated security=true";
            conn = new SqlConnection(cs);

        }


        public void DisplayMenu()
        {
            List<Course> cartList = CartContent();
            if (cartList.Count == 0)
            {
                Console.WriteLine("1. Course Categories");
                Console.WriteLine("2. View Cart");
                Console.WriteLine("0. Exit");
            }
            else
            {
                Console.WriteLine("1. Course Categories");
                Console.WriteLine("2. View Cart");
                Console.WriteLine("3. Generate Bill");
                Console.WriteLine("0. Exit");
            }

        }

        public void DisplayCategories()
        {
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("1. Web Development");
            Console.WriteLine("2. Investing and Trading");
            Console.WriteLine("3. 3D and Animation");
            Console.WriteLine("4. Fitness");
            Console.WriteLine("5. Musical Instruments");
            Console.WriteLine("------------------------------------------------");

        }

        public List<Course> GetCourseByCategory(string courseCat)
        {
            List<Course> categoryList = new List<Course>();
            string query = "select * from ProductTbl where Category = @courseCategory";
            cmd = new SqlCommand(query, conn);
            conn.Open();
            cmd.Parameters.AddWithValue("@courseCategory", courseCat);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Course course = new Course();
                course.Coursename = (string)reader["CourseName"];
                course.Author = (string)reader["Author"];
                course.Category = (string)reader["Category"];
                course.Price = (int)reader["Price"];
                course.Rating = (double)reader["Rating"];
                categoryList.Add(course);
            }
            conn.Close();
            return categoryList;

        }

        public void SelectedCourse(List<Course> selcCourse)
        {
            int c_index = 1;
            foreach (var course in selcCourse)
            {
                Console.WriteLine($"{c_index}: {course.Coursename} {course.Category}");
                Console.WriteLine("------------------------------------------------");
                c_index++;
            }
            Console.WriteLine("Select a course:");
            int selcCourseNum = int.Parse(Console.ReadLine());
            var selectedItem = selcCourse[selcCourseNum - 1];
            Console.WriteLine(selectedItem);

            CourseOptions(selectedItem);
            Console.WriteLine("---------------------------------------------------");
        }

        public void CourseOptions(Course selcItem)
        {
            List<Course> cartList = CartContent();
            if (cartList.Count == 0)
            {

                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("1. Add Course");
                Console.WriteLine("2. Buy Course");

                Console.WriteLine("Enter a choice: ");
                var buyCartOption = int.Parse(Console.ReadLine());
                if (buyCartOption == 1)
                {
                    Console.WriteLine("-----------------");
                    AddToCart(selcItem);

                }
                else if (buyCartOption == 2)
                {

                    Console.WriteLine($"The price for course ::{selcItem.Coursename}:: is Rs.{selcItem.Price}");
                    var gst_Price = selcItem.Price + (selcItem.Price * 0.18);
                    Console.WriteLine($"Order price with GST is Rs.{gst_Price}");
                }            
            }
            else
            {

                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("1. Add Course");
                Console.WriteLine("2. Buy Course");
                Console.WriteLine("3. Remove Course");


                Console.WriteLine("Enter a choice: ");
                var buyCartOption = int.Parse(Console.ReadLine());
                if (buyCartOption == 1)
                {
                    Console.WriteLine("-----------------");
                    AddToCart(selcItem);
                }
                else if (buyCartOption == 2)
                {

                    
                    Console.WriteLine($"The price for course ::{selcItem.Coursename}:: is Rs.{selcItem.Price}");
                    var gst_Price = selcItem.Price + (selcItem.Price * 0.18);
                    Console.WriteLine($"Order price with GST is Rs.{gst_Price}");
                }
                else if (buyCartOption == 3)
                {
                    //foreach (Course item in courseInCart)
                    //{
                    //    if (item.Coursename == selcItem.Coursename)
                    //    {
                    //        courseInCart.Remove(item);
                    //        Console.WriteLine($"{selcItem.Coursename} removed to cart successfully");
                    //    }

                    //}
                    //Console.WriteLine($"{selcItem.Coursename} does not exist in cart");
                    Console.WriteLine("-----------------");
                    RemoveFromCart(selcItem);
                }

                foreach (Course item in cartList)
                {
                    Console.WriteLine(item);
                }

            }

        }

        public List<Course> CartContent()
        {
            List<Course> cartList = new List<Course>();
            string query = "select * from CartTbl";
            cmd = new SqlCommand(query, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Course course = new Course();
                course.Coursename = (string)reader["CourseName"];
                course.Author = (string)reader["Author"];
                course.Category = (string)reader["Category"];
                course.Price = (int)reader["Price"];
                course.Rating = (double)reader["Rating"];
                cartList.Add(course);
            }
            conn.Close();
            return cartList;

        }

        public void AddToCart(Course course)
        {
            string Author = course.Author;
            string Category = course.Category;
            int Price = course.Price;
            double Rating = course.Rating;
            string query = "select * from CartTbl where CourseName = @coursename";
            cmd = new SqlCommand(query, conn);
            string CourseName = course.Coursename;
            cmd.Parameters.AddWithValue("@coursename", CourseName);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (!reader.HasRows)
            {
                conn.Close();
                query = $"Insert into CartTbl values(@coursename,@author,@category,@price,@rating)";
                cmd = new SqlCommand(query, conn);
                cmd.Parameters.AddWithValue("@coursename", CourseName);
                cmd.Parameters.AddWithValue("@author", Author);
                cmd.Parameters.AddWithValue("@category", Category);
                cmd.Parameters.AddWithValue("@price", Price);
                cmd.Parameters.AddWithValue("@rating", Rating);
                conn.Open();
                int insertResult = cmd.ExecuteNonQuery();
                if (insertResult > 0)
                {
                    Console.WriteLine("Course added successfully");
                    Console.WriteLine($"{course.Coursename}\t {course.Author}\t {course.Rating}\t {course.Category}\t Rs.{course.Price}");
                }
                else
                {
                    Console.WriteLine("Course not added");
                }
                conn.Close();

            }
            else
            {
                Console.WriteLine("Can't add values");
            }
            conn.Close();

        }

        public void RemoveFromCart(Course course)
        {
            string query = "Delete from CartTbl where CourseName = @courseName";
            cmd = new SqlCommand(query, conn);
            string CourseName = course.Coursename;
            cmd.Parameters.AddWithValue("@courseName", CourseName);
            conn.Open();
            int deletedResult = cmd.ExecuteNonQuery();
            if (deletedResult > 0)
            {
                Console.WriteLine("Course removed successfully");
            }
            else
            {
                Console.WriteLine("Course not removed");
            }
            conn.Close();
        }

        public void GenerateBill()
        {
            string query = "select SUM(Price) from CartTbl";
            cmd = new SqlCommand(query, conn);
            conn.Open();
            Object sum = cmd.ExecuteScalar();
            conn.Close();

            int total = 0;
            total += (int)sum;
            double totalGST_Price = total + (total * 0.8);
            Console.WriteLine($"Total price of items in cart:: Rs.{total}");
            Console.WriteLine($"Total price of items in cart with GST :: Rs.{totalGST_Price}");
            DeleteContent();
        }

        public void DeleteContent()
        {
            string query = "Truncate table CartTbl";
            cmd = new SqlCommand(query, conn);
            conn.Open();
            int deletetbl = cmd.ExecuteNonQuery();
            conn.Close();
            if (deletetbl > 0)
            {
                Console.WriteLine("The cart is empty");
            }
        }
    }
}
