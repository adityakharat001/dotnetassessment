﻿namespace ClassLibrary1
{
    public class Mobile
    {
        string company;
        string OS;
        int price;
        int year;

        public string GetAllDetails()
        {
            return $"{company}\t {OS}\t {price}\t {year}";
        }
    }
}