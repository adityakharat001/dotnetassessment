﻿using ClassLibrary2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotNetAppFramework
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mobile mobObj = new Mobile();
            Console.WriteLine("Enter the Mobile Company:");
            mobObj.company = Console.ReadLine();
            Console.WriteLine("Enter its operating system:");
            mobObj.OS = Console.ReadLine();
            Console.WriteLine("Enter the price of device:");
            mobObj.price = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the launch year");
            mobObj.year = int.Parse(Console.ReadLine());
            //Console.WriteLine($"Brand: {mobCompany}\nOperating System: {mobOS}\nPrice: {mobPrice}\nLanch-year: {launchYear} ");
            //Console.ReadLine();

            Console.WriteLine(mobObj.GetModelDetail());
            Console.ReadLine();
        }
    }
}
